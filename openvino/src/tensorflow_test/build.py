#!/usr/bin/env python3

import tensorflow as tf
import numpy as np

# 초기화 : 데이터 불러오기
data = np.loadtxt('./data.txt', delimiter=',', unpack=True, dtype='float32')
'''행렬 전치가 필요한경우의 예시
x_data = data[0:6].reshape(2, 3)
x_data = np.transpose(x_data, (3, 2))
'''
x_data = data[0:4] # [1, 2, 3, 4]
y_data = data[4:] # [2, 4, 6, 8]

X = tf.placeholder(tf.float32, name='x')
Y = tf.placeholder(tf.float32, name='y')

## 가중치와 바이어스 범위는 -100 ~ 100
W = tf.Variable(tf.random_uniform([1], -100., 100.), name='w')
b = tf.Variable(tf.random_uniform([1], -100., 100.), name='b')

mul = tf.multiply(W,X, name='mul')
hypothesis = tf.add(mul,b, name='hypothesis')

## hypothesis & cost func
cost = tf.reduce_mean(tf.square(hypothesis - Y), name='cost')

rate = tf.Variable(0.1)
optimizer = tf.train.GradientDescentOptimizer(rate)
train = optimizer.minimize(cost)

init = tf.initialize_all_variables()
sess = tf.Session()
sess.run(init)

op = sess.graph.get_operations()
for m in op:
	print(m.values())

saver = tf.train.Saver(max_to_keep=3)
for step in range(2001):
    sess.run(train, feed_dict={X: x_data, Y: y_data})
    if step % 20 == 0:
        print(step, sess.run(cost, feed_dict={X: x_data, Y: y_data}), sess.run(W), sess.run(b))

myFile = open('graph_def.txt','w')
myFile.write(str(sess.graph_def))
myFile.close()

output_node_names =[n.name for n in tf.get_default_graph().as_graph_def().node]
print(output_node_names)

saver.save(sess, './output/linear_regression.ckpt')
tf.io.write_graph(sess.graph_def, '.', './output/linear_regression.pb', as_text=False)
tf.io.write_graph(sess.graph_def, '.', './output/linear_regression.pbtxt', as_text=True)

print(sess.run(hypothesis, feed_dict={X: 5}))           # [ 10.]
print(sess.run(hypothesis, feed_dict={X: 2.5}))         # [5.]
print(sess.run(hypothesis, feed_dict={X: [2.5, 5]}))    # [  5.  10.], 원하는 X의 값만큼 전달.
