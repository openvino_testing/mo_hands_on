# -*- coding: utf-8 -*-
"""OpenVINO_Hands_On_gitlab.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1usQmYloA1vOihLk-_UrmrLvq5mOzEUEd

# Download Variables from Gitlab
"""

mo_hands_on = "/content/mo_hands_on"

!sudo apt-get install -y git-lfs

!rm -rf "{mo_hands_on}"

!git clone https://gitlab.com/openvino_testing/mo_hands_on.git
!cd "{mo_hands_on}" && \
    git lfs pull

!echo
!echo "Done"

!pwd
!cd "{mo_hands_on}"/openvino && ls -al

"""# Install the OpenVINO"""

## install tools
!sudo apt-get install -y pciutils cpio
!sudo apt autoremove

path = "/content/mo_hands_on/openvino"
zip_file = "l_openvino_toolkit_p_2021.1.110"

# unzip the openvino
!cd "{path}" && \
    rm -rf "{zip_file}" && \
    tar -xvzf "{zip_file}.tgz"

# install openvino
!cd "{path}"/"{zip_file}" && \
    sed -i 's/decline/accept/g' silent.cfg && \
    sudo ./install.sh --silent silent.cfg

## install dependencies
!sudo -E /opt/intel/openvino_2021/install_dependencies/install_openvino_dependencies.sh

## install prerequisites
!sudo /opt/intel/openvino_2021/deployment_tools/model_optimizer/install_prerequisites/install_prerequisites.sh   

!echo
!echo "Done"

"""# Run the Sample Demo [Optional]"""

## run demo
!source /opt/intel/openvino_2021/bin/setupvars.sh && \
    /opt/intel/openvino_2021/deployment_tools/demo/demo_squeezenet_download_convert_run.sh

"""# For Hands On Session

## TensorFlow (***Yolo v2***)
"""

# set basic path
path = "/content/mo_hands_on/openvino"
variable =  path + "/variables"
mo_path = "/opt/intel/openvino_2021/deployment_tools/model_optimizer"

"""### 1. Install the ***Darkflow***"""

yolo_v2_path = path + "/yolo_v2"
test_file = "darkflow"

!rm -rf "{yolo_v2_path}"
!mkdir -p "{yolo_v2_path}"

!cd "{yolo_v2_path}" && \
    git clone https://github.com/thtrieu/darkflow.git

!cd "{yolo_v2_path}"/"{test_file}" && \
    python3 setup.py build_ext --inplace && \
    ls -al ./flow

!echo
!echo "Done"

"""### 2. Convert **DarkNet** Model to **TensorFlow** Model"""

command = path + "/yolo_v2/darkflow"

# Create the pb file
!cd "{command}" && \
    python3 ./flow \
        --model cfg/yolo.cfg \
        --load "{variable}"/yolov2.weights \
        --savepb

!cd "{command}" && \
    mv built_graph/yolo.pb "{variable}"/yolo_v2.pb

!echo
!echo "Done"

"""### 3. Convert **TensorFlow** Model to **IR** files"""

# Conver yolo v2
!cd "{mo_path}" && \
    python3 mo.py \
        --input_model "{variable}"/yolo_v2.pb \
        --tensorflow_use_custom_operations_config extensions/front/tf/yolo_v2.json \
        --batch 1 \
        --output_dir "{variable}"

!echo
!echo "Done"

!cd "{variable}" && \
    ls -al yolo_v2*

"""## TensorFlow (***Yolo v3, Yolo v3 Tiny***)"""

yolo_v3_path = path + "/yolo_v3"
test_file = "tensorflow-yolo-v3"

!rm -rf "{yolo_v3_path}"
!mkdir -p "{yolo_v3_path}"

!cd "{yolo_v3_path}" && \
    git clone https://github.com/mystic123/tensorflow-yolo-v3.git

!cd "{yolo_v3_path}"/"{test_file}" && \
    git checkout ed60b90 && \
    git log -n 1

!echo
!echo "Done"

"""### 1. Dump **Yolo v3 TensorFlow** Model"""

command = path + "/yolo_v3/tensorflow-yolo-v3"

# Dump yolo v3
!cd "{command}" && \
    python3 convert_weights_pb.py \
        --class_names "{variable}"/coco.names \
        --data_format NHWC \
        --weights_file "{variable}"/yolov3.weights && \
    mv *.pb "{variable}"/yolo_v3.pb

# Dump yolo v3 tiny
!cd "{command}" && \
    python3 convert_weights_pb.py \
        --class_names "{variable}"/coco.names \
        --data_format NHWC \
        --weights_file "{variable}"/yolov3-tiny.weights \
        --tiny && \
    mv *.pb "{variable}"/yolo_v3_tiny.pb

!echo
!echo "Done"

"""### 2. Convert **TensorFlow** Model to **IR** files"""

# Convert yolo v3
!cd "{mo_path}" && \
    python3 mo.py \
        --input_model "{variable}"/yolo_v3.pb \
        --tensorflow_use_custom_operations_config extensions/front/tf/yolo_v3.json \
        --batch 1 \
        --output_dir "{variable}"

# Convert yolo v3 tiny
!cd "{mo_path}" && \
    python3 mo.py \
        --input_model "{variable}"/yolo_v3_tiny.pb \
        --tensorflow_use_custom_operations_config extensions/front/tf/yolo_v3_tiny.json \
        --batch 1 \
        --output_dir "{variable}"        

!echo
!echo "Done"

!cd "{variable}" && \
    ls -al yolo_v3.* && \
    echo && \
    ls -al yolo_v3_tiny*

"""## ONNX (***ResNet***)

### 1. Download **ONNX** Model (Opt)
"""

!cd "{variable}" && \
    wget https://zenodo.org/record/2592612/files/resnet50_v1.onnx && \
    wget https://zenodo.org/record/3228411/files/resnet34-ssd1200.onnx

!echo
!echo "Done"

"""### 2. Convert **ONNX** Model to **IR** files"""

# Convert ONNX(ResNet34)
!cd "{mo_path}" && \
    python3 mo.py \
        --input_model "{variable}"/resnet50_v1.onnx \
        --input_shape [1,3,800,800] \
        --output_dir "{variable}"

# Convert ONNX(ResNet50)
!cd "{mo_path}" && \
    python3 mo.py \
        --input_model "{variable}"/resnet34-ssd1200.onnx \
        --output_dir "{variable}"        

!echo
!echo "Done"

!cd "{variable}" && \
    ls -al resnet50* && \
    echo && \
    ls -al resnet34*